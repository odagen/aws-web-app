data "template_file" "app_cloud_config" {
  template = file("app_cloud_config.yaml")

  vars = {
    environment_tag = var.environment_tag
    inventory_code_tag = var.inventory_code_tag
    image_repository = var.image_repository
    image_build_tag = var.image_build_tag
    ec2_log_dir = var.ec2_log_dir
    docker_username = var.docker_username
    docker_password = var.docker_password
  }
}

data "aws_ami" "coreos" {
  most_recent = true

  filter {
    name = "name"
    values = ["CoreOS-stable-*"]
  }

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["595879546273"]
}

resource "aws_alb" "app_lb" {
  name_prefix = "app-lb"
  subnets = aws_subnet.public-subnet.*.id
  security_groups = [aws_security_group.lb-sg-http.id]
  internal = false
  idle_timeout = 300

  access_logs {
    bucket = aws_s3_bucket.s3_logs_bucket.bucket
    prefix = "logs"
    enabled = "true"
  }

  tags = {
    Environment = var.environment_tag
    InventoryCode = var.inventory_code_tag
  }
}

resource "aws_alb_listener" "app_lb_http_listener" {
  load_balancer_arn = aws_alb.app_lb.arn
  port = var.app_alb_port_http
  protocol = var.app_alb_protocol

  default_action {
    target_group_arn = aws_lb_target_group.app_target_group.arn
    type = "forward"
  }
}

resource "aws_lb_target_group" "app_target_group" {
  name_prefix     = "app-tg"
  port     = var.app_instance_port
  protocol = var.app_instance_protocol
  vpc_id   = aws_vpc.app-vpc.id

  health_check {
    interval            = "30"
    healthy_threshold   = "5"
    unhealthy_threshold = "2"
    timeout             = "10"
    protocol            = var.app_instance_protocol
    path                = var.app_instance_healthcheck_url
    port                = var.app_instance_port
    matcher             = var.app_instance_response_codes
  }
}

resource "aws_launch_configuration" "app_lc" {
  depends_on = [aws_iam_instance_profile.app_instance_profile]

  name_prefix = "app-lc-"
  image_id = data.aws_ami.coreos.image_id
  instance_type = var.app_instance_type
  enable_monitoring = var.enable_monitoring
  user_data = data.template_file.app_cloud_config.rendered
  security_groups = [
    aws_security_group.instance-sg-http.id,
    aws_security_group.instance-sg-ssh.id,
    aws_security_group.instance-sg-journal-gatewayd.id,
    aws_security_group.instance-sg-ntp.id
  ]
  associate_public_ip_address = "true"
  key_name = var.app_ssh_key

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "app_asg" {
  name_prefix = "app-asg-"
  min_size = var.app_instance_min_count
  max_size = var.app_instance_max_count
  desired_capacity = var.app_instance_desired_count
  launch_configuration = aws_launch_configuration.app_lc.id
  target_group_arns = [aws_lb_target_group.app_target_group.arn]
  vpc_zone_identifier = [join(",", aws_subnet.public-subnet.*.id)]
  termination_policies = [var.asg_termination_policies]
  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances"]
  wait_for_capacity_timeout = "5m"
  health_check_grace_period = 600
  health_check_type = "ELB"
  min_elb_capacity = "1"
  force_delete = true

  lifecycle {
    create_before_destroy = true
  }
}