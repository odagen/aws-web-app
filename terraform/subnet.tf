resource "aws_subnet" "public-subnet" {
  count = length(var.availability_zones)
  vpc_id = aws_vpc.app-vpc.id
  cidr_block = lookup(var.public_subnets, var.availability_zones[count.index])
  availability_zone = var.availability_zones[count.index]

  tags = {
    Name = "Public zone"
  }
}

resource "aws_route_table" "route-table-int-gw" {
  vpc_id = aws_vpc.app-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.int-gw.id
  }

  tags = {
    Name = "int-gw-route-table"
  }
}

resource "aws_route_table_association" "subnet-association" {
  count = length(aws_subnet.public-subnet)
  subnet_id      = aws_subnet.public-subnet[count.index].id
  route_table_id = aws_route_table.route-table-int-gw.id
}