variable "aws_region" {}

variable "docker_username" { default = "" }
variable "docker_password" { default = "" }

variable "cidr_block" {
  description = "CIDR for the whole VPC"
  default = "10.0.0.0/16"
}

variable "public_subnets" {
  description = "CIDR for the Public subnet"
  default = {
    eu-central-1a = "10.0.0.0/24",
    eu-central-1b = "10.0.1.0/24",
    eu-central-1c = "10.0.2.0/24"
  }
}

variable "availability_zones" {
  description = "Availability zones"
  default = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
}

variable "app_instance_type" {}
variable "app_alb_protocol" { default = "HTTP" }
variable "app_alb_port_http" { default = "80" }
variable "app_instance_healthcheck_url" { default = "/actuator/health" }
variable "app_instance_response_codes" { default = "200-299"}
variable "app_instance_protocol" { default = "HTTP" }
variable "app_instance_port" { default = "8080" }
variable "app_instance_min_count" {}
variable "app_instance_max_count" {}
variable "app_instance_desired_count" {}
variable "app_ssh_key" {}
variable "asg_termination_policies" { default = "OldestLaunchConfiguration" }
variable "enable_monitoring" {}
variable "ec2_log_dir" {}

variable "inventory_code_tag" {}
variable "environment_tag" {}

variable "image_build_tag" { default = "latest" }
variable "image_repository" { default = "docker.io/desh" }